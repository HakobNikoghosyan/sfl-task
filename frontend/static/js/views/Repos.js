import AbstractView from "./AbstractView.js";

export default class extends AbstractView {
  constructor(params) {
    super(params);
    this.setTitle("Repos");
  }

   getHtml() {
    return `
            <h1>Repos</h1>
            <p>You are viewing the Repos!</p>
        `;
  }
}
